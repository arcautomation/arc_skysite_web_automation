package com.skysite.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.base.Function;

import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.StopWatch;

/**
 * Added by Subhendu Das on 17/09/2017.
 * These are reusable methods can be used across SkySite Automation project.
 */

public class SkySiteUtils{
	
	public static boolean waitForElement(WebDriver driver, WebElement element, int maxWait)
	{
		boolean statusOfElementToBeReturned = false;
        long startTime = StopWatch.startTime();
        WebDriverWait wait = new WebDriverWait(driver, maxWait);
        try {
            WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
            if (waitElement.isDisplayed() && waitElement.isEnabled()) {
                statusOfElementToBeReturned = true;
                Log.event("Element is displayed:: " + element.toString());
            }
        } catch (Exception e) {
            try {
				throw new Exception("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
        }
        return statusOfElementToBeReturned;
    }
	
	public static void waitTill(int value){
		   try {
			     Thread.sleep(value);
		    } catch (InterruptedException e) {
			     e.printStackTrace();
		      }
	   }
	}
	

    
