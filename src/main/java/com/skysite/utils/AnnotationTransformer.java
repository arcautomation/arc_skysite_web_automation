package com.skysite.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.annotations.ITestAnnotation;
import org.testng.internal.annotations.IAnnotationTransformer;

public class AnnotationTransformer implements IAnnotationTransformer {
    @Override
    public void transform(ITestAnnotation arg0, Class arg1, Constructor arg2, Method arg3) {
        int invocationCount = 0;
        if (null != System.getenv("invocationCount")) {
            invocationCount = Integer.parseInt(System.getenv("invocationCount"));
        } else {
            invocationCount = 1;
        }

        String[] groups = arg0.getGroups();
        for (int index = 0; index < groups.length; index++) {
            if (groups[index].contains("services")) {
                arg0.setInvocationCount(invocationCount);
                break;
            }
        }
    }
}