package com.skysite.pages;



import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arcautoframe.utils.Log;
import com.skysite.utils.PropertyReader;
import com.skysite.utils.SkySiteUtils;

public class ProjectDashboardPage  extends LoadableComponent<ProjectDashboardPage> {

	WebDriver driver;
	private boolean isPageLoaded;
	
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	
	@FindBy(css=".dropdown.sec-pref.open>a")
	WebElement btnLogOff;
	
	 //@FindBy(css="div.modal-footer .btn.btn-default")
	@FindBy(xpath=".//*[@id='WhatsNewFeature']/div/div/div[3]/button[1]")
	WebElement btnSkip;
	
	//@FindBy(css=".close")
	@FindBy(xpath=".//*[@id='WhatsNewFeature']/div/div/div[1]/button")
	WebElement btnClose;
	
	@FindBy(css=".ltmenu-user.badge-stack.dropdown>a")
	WebElement btnProfile;
	
	@FindBy(css="#add-project")
	WebElement btnCreateProject;
	
	@FindBy(css="#btnCreate")
	WebElement btnCreate;
	
	@FindBy(css="#txtProjectName")
	WebElement txtProjectName;
	
	@FindBy(css="#txtProjectNumber")
	WebElement txtProjectNumber;
	
	@FindBy(css="#txtProjectDescription")
	WebElement txtProjectDescription;
	
	@FindBy(css="#txtAddress")
	WebElement txtAddress;
	
	@FindBy(css="#txtProjectStartDate")
	WebElement txtProjectStartDate;
	
	@FindBy(css="#txtCity")
	WebElement txtCity;
	
	@FindBy(css="#txtState")
	WebElement txtState;
	
	@FindBy(css="#txtZip")
	WebElement txtZip;
	
	@FindBy(css="#txtCountry")
	WebElement txtCountry;
	
	@FindBy(css="#txtProjectPassword")
	WebElement txtProjectPassword;
	
	@FindBy(css=".noty_text")
	WebElement projectCreationSuccessMsg;
	
	@FindBy(css=".Country-Item[title='USA']")
	WebElement countryTitleItem;
	
	@FindBy(css=".State-Item[data-name='alabama']")
	WebElement stateTitleItem;
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		//SkySiteUtils.waitForElement(driver, btnLogOff, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public ProjectDashboardPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	/** 
	 * Method written for checking the availability of Profile button.
	 * @return
	 */
	public boolean presenceOfProfileButton()
	{
		SkySiteUtils.waitTill(8000);
		String Parent_Window = driver.getWindowHandle(); 
		for (String Child_Window : driver.getWindowHandles())  
	     {  
	     driver.switchTo().window(Child_Window); 
	     JavascriptExecutor js = (JavascriptExecutor)driver;
	     Boolean btnCloseIsPresent = driver.findElements(By.xpath(".//*[@id='WhatsNewFeature']/div/div/div[1]/button")).size()>0;
	     if(btnCloseIsPresent.equals(true))
	     {
	     js.executeScript("arguments[0].click();", btnClose);
	     Log.message("Modal window is closed.");
	     }
	    /* else
	     {
	     SkySiteUtils.waitTill(10000);
	 	 driver.switchTo().window(Parent_Window); 
	     } */
	     }
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		Log.message("Checking whether Profile button is present?");
		if(btnProfile.isDisplayed())
			return true;
		else
			return false;
		
	}
	
	/** 
	 * Method written for creating new project.
	 * @return
	 */
	public boolean createProject()
	{
		SkySiteUtils.waitForElement(driver, btnCreateProject, 20);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(5000);
		String Parent_Window = driver.getWindowHandle(); 
		ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(windowHandles.get(1));
		txtProjectName.clear();
		txtProjectName.sendKeys(PropertyReader.getProperty("Projectname"));
		Log.message("Project Name has been entered.");
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry, 20);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(3000);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 20);
		stateTitleItem.click();
		Log.message("State has been selected.");
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		txtProjectPassword.clear();
		txtProjectPassword.sendKeys(PropertyReader.getProperty("Projectpassword"));
		Log.message("Project Password has been entered.");
		SkySiteUtils.waitTill(5000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		driver.switchTo().window(Parent_Window);
		SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 20);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		String actualProjSuccessMsg = "Project created successfully";
		if(expectedProjSuccessMsg.equalsIgnoreCase(actualProjSuccessMsg))
			return true;
			else
			return false;	
	}
	
	/** 
	 * Method written for deleting existing project.
	 * @return
	 */
	/*public boolean deleteProject()
	{
		SkySiteUtils.waitForElement(driver, btnCreateProject, 20);
	}*/

}
