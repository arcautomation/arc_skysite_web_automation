package com.skysite.pages;


import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;





import com.arcautoframe.utils.Log;
import com.skysite.utils.SkySiteUtils;

public class SplashPage extends LoadableComponent<SplashPage>{
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	
	@FindBy(css=".head-signin>a")
	WebElement btnSignIn;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSignIn, 40);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public SplashPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	
	/**
	 * Method written for clicking on SignIn button.
	 * @return
	 */
	public LoginPage clickOnSignIn()
	{
		Log.message("Waiting for 'SignIn' button to be appeared");
		SkySiteUtils.waitForElement(driver, btnSignIn, 30);
		Log.message("'SignIn' button is appeared");
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", btnSignIn);
		//btnSignIn.click();
		Log.message("'SignIn' button is clicked");
		ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(windowHandles.get(1));
		return new LoginPage(driver).get();
	}
	
	
	/**
	 * Method written for checking whether SkySite Application launched successfully?
	 * @return
	 */
	public boolean skySiteApplaunch()
	{
		Log.message("Waiting for 'SignIn' button to be appeared");
		SkySiteUtils.waitForElement(driver, btnSignIn, 40);
		if(btnSignIn.isEnabled())
		return true;
		else
			return false;
	}

	

}
