package com.skysite.testscripts;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;
import com.arcautoframe.utils.WebDriverFactory;
import com.skysite.pages.ProjectDashboardPage;
import com.skysite.pages.LoginPage;
import com.skysite.pages.SplashPage;

import com.skysite.utils.PropertyReader;

import com.skysite.utils.SkySiteAppLaunch;

@Listeners(EmailReport.class)

public class SkySiteRegressionSuite1{
	
	static WebDriver driver;
	
	WebDriverFactory webDriverFactory;
	String webSite;
	ITestResult result;
	SplashPage splashPage;
	LoginPage loginPage;
	ProjectDashboardPage homePage;
	
	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	  } 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001: Verify successful Application launch.
	 * 
	 * @throws Exception
	 */
	
	@Test(priority = 0, enabled = true, description = "TC_001: Verify successful Application launch.")
	public void verifyAppLaunch() throws Exception
	{
		Log.testCaseInfo("Verify successful Application launch.");
		//WebDriver driver = SkySiteAppLaunch.launchURL();
		//driver = webDriverFactory.browserLaunch("browser",PropertyReader.getProperty("SkysiteProdURL"));
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			Log.message("Splash page objects initiated successfully");
			Log.assertThat(splashPage.skySiteApplaunch(), "SkySite application lanched successfully.", "SkySite application does not launch successfully.", driver);
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002: Verify successful landing of Login Page.
	 * @throws Exception 
	 * 
	 */
	@Test(priority = 1, enabled = true, description = "TC_002: Verify successful landing of Login Page.")
	public void verifyLandingOfLoginPage() throws Exception
	{
		Log.testCaseInfo("Verify successful landing of Login Page.");
		//WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();	
			loginPage = splashPage.clickOnSignIn();
			Log.message("Sign In button is clicked");
			Log.assertThat(loginPage.presenceOfTextBoxUserName(), "LogIn page landed successfully.", "LogIn page is not landed successfully.", driver);
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003: Verify login with valid credential.
	 * 
	 */
	@Test(priority = 2, enabled = false, description = "TC_003: Verify login with valid credential.")
	public void verifyLoginWithValidCredential() throws Exception
	{
		Log.testCaseInfo("Verify successful landing of Project Creation Page.");
		//WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			loginPage = splashPage.clickOnSignIn();
			homePage = loginPage.loginWithValidCredential();
			Log.assertThat(homePage.presenceOfProfileButton(), "Login successful with valid credential.", "Login unsuccessful with valid credential.", driver);
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004: Verify login with invalid credential.
	 * 
	 */
	@Test(priority = 3, enabled = false, description = "TC_004: Verify login with invalid credential.")
	public void verifyLoginWithInvalidCredential() throws Exception
	{
		Log.testCaseInfo("Verify Login would be failed with Invalid credentials.");
		//WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			loginPage = splashPage.clickOnSignIn();
			Log.assertThat(loginPage.loginWithInvalidCredential(), "Login unsuccessful with invalid credential", "Login successful with invalid credential.", driver);
		}	
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_005: Verify project creation.
	 * 
	 */
	@Test(priority = 4, enabled = false, description = "TC_005: Verify project creation.")
	public void verifyProjectCreation() throws Exception
	{
		Log.testCaseInfo("Verify project creation.");
		//WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			loginPage = splashPage.clickOnSignIn();
			homePage = loginPage.loginWithValidCredential();
			homePage.presenceOfProfileButton();
			Log.assertThat(homePage.createProject(), "Project created successfully.", "Project creation failed", driver);
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
}
